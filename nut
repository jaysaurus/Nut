#! /bin/bash
# core of nut
str1=""
while getopts :n name
do 
	case $name in
		n)nopt=1;;
		*)xopt=1
	esac
done

bash /usr/local/bin/nutShell/hasFile.sh
# Shell ternary-like operator.  Manages the presence of a valid option.
action=$([ -z $nopt ] && echo "$1" || echo "$2") 
case $action in
	"get")
	bash /usr/local/bin/nutShell/get.sh "$2"
	;;
	"list")
	bash /usr/local/bin/nutShell/list.sh
	;;
	"add")  
	if [ ! -z $nopt ]
		then
		bash /usr/local/bin/nutShell/addN.sh # Add a nut
		else
		bash /usr/local/bin/nutShell/add.sh # Add a note
	fi
	;;
	"del") # del can be passed an optional line number.
	if [ ! -z $nopt ]
		then
		bash /usr/local/bin/nutShell/delN.sh # Delete a nut
		else	
		if [ -n "$2" ] # Delete a note has an additional line argument
			then
			bash /usr/local/bin/nutShell/del.sh "$2"
			else
			bash /usr/local/bin/nutShell/del.sh
		fi
	fi
	;;
	*) # ERROR HANDLING
	if [ -n "$1" ]
		then
		if [ ! -z $nopt ]
			then
			echo "-n must be followed by \"add\" or \"del\""
			else
			if [ -z $xopt ]
				then
				echo "invalid command"
				else
				echo "invalid option"
			fi
		fi
		exit 2
		else # by default nut will read the current note using the cracker program below
		bash /usr/local/bin/nutShell/cracker.sh		
	fi
	exit 0
	;;
esac
