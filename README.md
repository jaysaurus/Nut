# nut
### A StickyNotes-style program for Bash.

add notes - or - "nuts" that are quick and easy to access.  No frills, no fuss!

1. simply add the shell and the folder to /usr/local/bin (making sure that the "nut" file is sat directly in bin beside its respective nutShell file)

2. then type "nut" at command line to run/install program.

## Commands

**nut** - View the currently selected nut (default.nut by default)

**nut add** - Add to the currently selected nut

**nut del 1** - Remove item #1 from your current nut

**nut get <nut-name>** - load requested nut name

**nut list** - list all nuts

**nut -n del** - run nut deletion tool

**nut -n add** - run nut creation tool
