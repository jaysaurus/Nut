#! /bin/bash
# *** DELETE A FILE ***
bash list.sh
cd ~
echo "Enter the name of the Nut to delete (without path or file extension):"
read nutName
if [ ! -z $nutName ]
	then
	current=".nut/notes/$nutName.nut"
	if [ -e $current ]
		then
		rm $current # wipe selected
		rm .nut/.nut.conf # wipe current pointer
		bash /usr/local/bin/nutShell/hasFile.sh # set current.tmp et al back to default.#
		echo "Nut $nutName deleted."
		else
		echo "Nut not found."
		exit 2
	fi
fi
exit 0
