#! /bin/bash
# *** NUT CRACKER ***
# This file prints out the currently selected note.

# validates that the current nut && nut.conf exists. 
cd ~
defaultCurrent() {
	echo $1
	
	echo ".nut/notes/default.nut" > .nut/.nut.conf
	if [ ! -e .nut/notes/default.nut ]
		then
		touch .nut/notes/default.nut
	fi
}

getTitle() {
	echo
	echo $(basename "$1")
	echo "~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~"
}
# 1) does the nut.conf file exist? 
if [[ -e .nut/.nut.conf ]] 
	then # 2) get the path to the current note
	cur=$(cat .nut/.nut.conf)
	i=0
	if [ -e $cur ] # 3) check that the current note exists
		then
		cat $cur | { while read ITEM # 4) iterate through each note...
			do
			if (( i < 1 ))
				then
				getTitle $cur
			fi
			(( i++ ))
			echo "$i) $ITEM"
			echo "- - - - -"
			done 
		if (( i == 0 )) # ... or alert user if the note happens to be empty
			then 
			getTitle $cur
			echo "* empty *"
			echo "- - - - -"
		fi			
		} | more
		else
		defaultCurrent "$cur is missing or invalid, nut will be set to default"
	fi
	else
	defaultCurrent "the nut config file is missing or invalid, nut will be set to default.nut"
	bash hasFile.sh
fi
