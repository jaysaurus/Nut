#! /bin/bash
# VALIDATE THAT A FILE EXISTS
cd ~
if [ ! -d .nut ]
	then
	mkdir .nut
	mkdir .nut/notes
fi
if [ ! -e .nut/.nut.conf ]
	then
	touch .nut/notes/default.nut
	echo .nut/notes/default.nut >  .nut/.nut.conf | cat
fi
if [ ! -e `cat .nut/.nut.conf` ]
	then 
	touch `cat .nut/.nut.conf`
fi
