#! /bin/bash
# LIST ALL THE PRESENT NOTE FILES
# 1) pipe simple ls, 
# 2) remove file extensions, 
# 3) iterate through ls and precede names with a number
# 4) supply formatted results to more
i=1
ls ~/.nut/notes | cut -f1 -d'.' | while read ln 
	do 
	echo "nut $i) $ln"
	let i++
	done | more
