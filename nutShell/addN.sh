#! /bin/bash
# *** ADD A FILE ***
cd ~
echo "Enter a name for your new Nut:"
read nutName
current=".nut/notes/"$nutName".nut"
if [ ! -e $current ]
	then
	touch $current	
	echo "$current" > ~/.nut/.nut.conf 
	echo "nut $nutName created and set to current."
fi

exit 0
