#! /bin/bash
# DELETE LINE
# del($1 = file, $2 = line)
cd ~
nutDel() {
	i=1
	#LINES="$(< $1)"
	#for LINE in $LINES; do
	while read LINE
	do
		if  (( i == $2 ))
			then
			# perform delete task
			sed -i -e "$2d" $1
			cur=$(basename "$1") # remove file path
			cur="${cur%%.*}" # remove file extension
			echo "deleted from $cur nut: $2) $LINE"
		 	exit 0
			else			
			((i++))
		fi 
	done < $1
}

# Execution
if [ -n "$1" ]
	then
	current=`cat .nut/.nut.conf`
	line=`wc -l $current | cut -f1 -d' '`
	# validate that a number has been passed and that the number is <= lines of file
	if [ $1 -ne 0 -o $1 -eq 0 2>/dev/null ] 
		then
			if [ $1 -le $line ]
				then # call function above
				nutDel $current $1 
				else
				echo "number is greater than number of notes in the nut"
			fi
		else
		echo "you didn't send a valid number"
	fi
	else
	echo "you didn't pass del any data"
fi
