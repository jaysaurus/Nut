#! /bin/bashi
# get a different Nut instance
cd ~
if [ ! -z $1 ]
	then
	 
 	get=".nut/notes/"$1".nut"
	if [ -e $get ]
		then
		echo $get > .nut/.nut.conf
		echo "$1 loaded."
		bash /usr/local/bin/nutShell/cracker.sh	
		else
		echo "Nut $1 not found"
	fi
	else
	echo "\"get\" needs to be passed with a Nut name. use \"nut list\" to view available Nuts."
fi
